
## ERA5 HydroForcing variables (hourly)

```r
#           var                      longname                  unit
#  1: time_bnds                     time_bnds
#  2:       10u     10 metre U wind component               m s**-1
#  3:       10v     10 metre V wind component               m s**-1
#  4:        2d  2 metre dewpoint temperature                     K
#  5:        2t           2 metre temperature                     K
#  6:         e                   Evaporation m of water equivalent
#  7:       pev         Potential evaporation                     m
#  8:       ssr   Surface net solar radiation               J m**-2
#  9:       str Surface net thermal radiation               J m**-2
# 10:        sp              Surface pressure                    Pa
# 11:        tp           Total precipitation                     m
```
