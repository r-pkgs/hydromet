## Variables

- `Rn`      : 地表净辐射，(MJ m-2 d-1)
- `EVP`     : 蒸发皿蒸发，(0.1mm)
- `TG`      : 地表温度，(0.1℃)
- `Tair`    : 地表大气温度，(0.1℃)
- `Prcp`    : 降水，(0.1mm)
- `Pa`      : 大气压，(10 Pa)
- `RH`      : 相对湿度，(%)
- `SSD`     : 光照时数，(0.1 hour)
- `WIN_avg` : 地表平均风速，(0.1 m s-1)
