# library(sf.extract)
library(sf)
library(terra)
library(lubridate)
library(magrittr)
library(data.table)
library(purrr)
source('scripts/main_pkgs.R')

## study region
shp <- "data-raw/bou2_4p_ChinaProvince.shp"
st <- sf::read_sf(shp)

## 01. ERA5 prcp ---------------------------------------------------------------
infile = "data-raw/HydroForcing/ERA5_China_HydroForcing_202101-202111.nc"
# r = rast(infile, subds = "tp") * 1000 * 24
r = rast(infile)
df_ERA5 = st_extract_rast(r, st)
d_ERA5 = df_ERA5[name == "山西省"]
plot(d_ERA5$time, d_ERA5$tp * 1000 * 24, type = "l")

## 02. GSMap prcp --------------------------------------------------------------
files = dir("data-raw/HydroForcing/GSMap_daily", "*.tif", full.names = T)
r = rast(files)
dates = seq(make_date(2021, 6, 1), make_date(2021, 10, 31), by = "day")
terra::time(r) = dates

df_GSMap = st_extract_rast(r, st, vars = "prcp")
d_GSMap = df_GSMap[name == "山西省"]
plot(d_GSMap$time, d_GSMap$prcp, type = "l")


## visualization
{
    period = c(make_date(2021, 6, 1), make_date(2021, 10, 31))

    par(mfrow = c(1, 2), mar = c(4, 4, 1, 1))
    plot(d_ERA5$time, d_ERA5$tp * 1000 * 24, type = "l", xlim = period, )
    grid()

    plot(d_GSMap$time, d_GSMap$prcp, type = "l", xlim = period)
    grid()
}

# library(ggplot2)
# ggplot(dat, aes(time, `山西省`)) +
#     geom_line()
# plot(r[[1:2]] * 1000 * 24)
# plot(vect(shp), add = TRUE)
# region = subset(st, NAME == "山西省")
