library(data.table)
library(magrittr)
library(hydroTools)
library(ggplot2)

# d = fread("C:/Users/kongdd/Desktop/INPUT_武汉PET/INPUT_57494武汉_气象输入_1951-2019.csv")
infile = "data-raw/Mete/processed_59287_广州.csv"
outfile = "PET2019_广州.pdf"
lat = 23.13333 # 广州

# infile = "data-raw/Mete/processed_57494_武汉.csv"
# outfile = "PET2019_武汉.pdf"
# lat = 30.63333 # 武汉

# infile = "data-raw/Mete/processed_54511_北京.csv"
# outfile = "PET2019_北京.pdf"
# lat = 39.8 # 北京

d = fread(infile)
d$date = as.Date(d$date)
d = d[date >= "2019-01-01"]

dat = d[, .(
    date,
    # Rn,
    # Rn = MJ_2W(Rn),
    Tair_max = Tair_max/10,
    Tair_min = Tair_min/10,
    Tair_avg = Tair_avg/10,
    Pa       = Pa_avg/100, # kPa
    RH       = RH_avg,
    SSD      = SSD/10,
    wind     = WIN_Avg/10,
    EVP_bg   = EVP_bg/10,
    EVP_sm   = EVP_bg/10,
    prcp = `Prcp_20-20`/10
)]

dat_Ra = dat[, .( date, cal_Rn(lat, yday(date), SSD, RH, Tair_min, Tair_max)) ]
dat$Rn = dat_Ra$Rn

es = dat[, (cal_es(Tair_max) + cal_es(Tair_min))/ 2] # eq. 12
ea = es * dat$RH/100# eq. 19
D = es - ea

res_water = dat[, .(date, ET0_PM93(MJ_2W(Rn), Tair_avg, D, Pa, wind = wind, z.wind = 2))]
res_crop  = dat[, .(date, ET0_PM98(MJ_2W(Rn), Tair_avg, D, Pa, wind = wind, z.wind = 2))]
res_crop = cbind(res_crop, ET0_PM93 = res_water$ET0_PM93)

gof_pm98 = GOF(d$EVP_bg/10, res_crop$ET0_PM98)
GOF(d$EVP_bg/10, res_crop$Eeq_pm * 1.26) # gamma differ

gof_pm93 = GOF(d$EVP_bg/10, res_water$ET0_PM93)
GOF(d$EVP_bg/10, res_water$Eeq * 1.26)

{
    p1 <- ggplot(res_crop, aes(date, ET0_PM98)) +
        geom_line(data = d, aes(y = EVP_bg/10, color = "Pan")) +
        geom_line(aes(color = "Penman Monteith 1998")) +
        geom_line(aes(y = ET0_PM93, color = "Penman 1993")) +
        # geom_line(aes(y = res_water$Eeq * 1.26, color = "Priestley Taylor 1972")) +
        theme(legend.position = c(0.01, 1 - 0.01),
              legend.justification = c(0, 1), legend.title = element_blank()) +
        scale_color_manual(values = alpha(c("black", "blue", "green", "red"), 0.8)) +
        labs(x = NULL, y = "Potential Evapotranspiration (mm/d)")
    period = c(make_date(2019, 6, 1), make_date(2019, 8, 31))
    p2 = p1 + scale_x_date(limits = period) +
        theme(legend.position = "none")
    p = p1 + p2 + plot_layout(widths = c(2, 1))

    write_fig(p, outfile, 12, 5)
}

# library(gggrid)
# tg <- textGrob(expression(R^2),
#                x=unit(1, "npc") - unit(2, "mm"),
#                y=unit(1, "npc") - unit(2, "mm"),
#                just=c("right", "top"))
# p2 +  grid_panel(tg)
# p2 + annotate(geom = "text", x = -Inf, y = Inf, hjust = 0, vjust = 1, label = "(a)")
